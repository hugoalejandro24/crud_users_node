import userModel from '../models/userModel.js';
import userViewModel from '../models/userViewModel.js';
import validateParams from '../helpers/validateParams.js';
import userTypeModel from '../models/userTypeModel.js';
import { Op } from 'sequelize';
import jwt from 'jsonwebtoken';
import { v4 as uuidv4 } from 'uuid';
import moment from "moment";
import { invalidParams } from '../responses/generic_responses.js';
import { successIndex, successShow, successLogin, notFound, successCreate, failCreate, successUpdate, successDelete, failLogin } from '../responses/user/responses.js';
import CONSTANTS from "../config/constants.js";

const index = async (req, res) => {
    const users = await userViewModel.findAll();
    return res.status(200).json(successIndex(users));
};

const show = async (req, res) => {
    const user = await userViewModel.findOne({where:{id:req.params.id}});
    if(!user) { return res.status(404).json(notFound({id:req.params.id})); }
    return res.status(200).json(successShow(user));
};

const create = async (req, res) => {
    let data = await validateCreate(req, res);
    data.id = uuidv4();
    const user = await userModel.create(data);
    if(!user) return res.status(500).json(failCreate(user));
    const userCreated = await userViewModel.findOne({where:{id:data.id}});
    return res.status(201).json(successCreate(userCreated));
};

const update = async (req, res) => {
    const data = await validateUpdate(req, res);
    await userModel.update(data, {where:{id:req.params.id}});
    const userUpdated = await userViewModel.findOne({where:{id:req.params.id}});
    return res.status(201).json(successUpdate(userUpdated));
};

const deleteSoft = async (req, res) => {
    const userExists = await userViewModel.findOne({where: {id:req.params.id}});
    if(!userExists) return res.status(404).json(notFound({id:req.params.id}));
    await userModel.update({
        deleted_at:moment().format("YYYY-MM-DD HH:mm:ss"),
        deleted_by:req.user.id
    }, {where:{id:req.params.id}});
    return res.status(201).json(successDelete({id:req.params.id}));
}

const destroy = async (req, res) => {
    const userExists = await userViewModel.findOne({where: {id:req.params.id}});
    if(!userExists) return res.status(404).json(notFound({id:req.params.id}));
    await userModel.destroy({where:{id:req.params.id}});
    return res.status(201).json(successDelete({id:req.params.id}));
};

const login = async (req, res) => {
    const user = await validateLogin(req, res);
    const user_view = await userViewModel.findOne({where:{id:user.id}});
    const payload = { id: user.id };
    const options = { expiresIn: CONSTANTS.DEBUG ? '24h' : '2h' };
    const token = jwt.sign(payload, CONSTANTS.JWT_SECRET_KEY, options);
    return res.status(201).json(successLogin(user_view, token))
};

const validateCreate = async (req, res) => {
    let data = validateParams([
        ["name", req.body.name || null, "string", true],
        ["user", req.body.user || null, "string", true],
        ["email", req.body.email || null, "string", true],
        ["password", req.body.password || null, "string", true],
        ["user_type_id", req.body.user_type_id || null, "string", true],
        ["created_by", req.user.id || null, "string", true]
    ]);
    if(data.error) return res.status(400).json(invalidParams("Invalid params", data.error));
    const emailExists = await userModel.findOne({where: {email:data.email}});
    if (emailExists) return res.status(400).json(invalidParams("Email already exists", {email: data.email}));
    const userTypeExists = await userTypeModel.findOne({where:{id:data.user_type_id}})
    if (!userTypeExists) return res.status(404).json(invalidParams("User type does not exists", {user_type_id: data.user_type_id}));
    return data
}

const validateUpdate = async (req, res) => {
    const data = validateParams([
        ["name", req.body.name || null, "string", true],
        ["user", req.body.user || null, "string", true],
        ["email", req.body.email || null, "string", true],
        ["user_type_id", req.body.user_type_id || null, "string", true],
        ["updated_by", req.user.id || null, "string", true]
    ]);
    if(data.error) return res.status(400).json(invalidParams("Invalid params", data.error));
    const userExists = await userViewModel.findOne({where: {id:req.params.id}});
    if(!userExists) return res.status(404).json(notFound({id:req.params.id}));
    const emailExists = await userViewModel.findOne({where: {email:data.email, id:{[Op.not]:req.params.id}}});
    if (emailExists) return res.status(400).json(invalidParams("Email already exists", {email: data.email}));
    const userTypeExists = await userTypeModel.findOne({where:{id:data.user_type_id}})
    if (!userTypeExists) return res.status(404).json(invalidParams("User type not found", {user_type_id: data.user_type_id}));
    return data
}

const validateLogin = async (req, res) => {
    const data = validateParams([
        ["user", req.body.user || null, "string", true],
        ["password", req.body.password || null, "string", true]
    ]);
    if(data.error){ return res.status(400).json(invalidParams("Invalid params", data.error)); }
    const { user, password } = data;
    const userExists = await userModel.findOne(
        {where:{
            [Op.or]:[{user},
            {email:user}],
            password,
            is_active:1,
            deleted_at:null
        }}
    );
    if(!userExists) return res.status(401).json(failLogin());
    return userExists
}

export {
    deleteSoft,
    create,
    login,
    index,
    show,
    update,
    destroy
};