import { DataTypes } from "sequelize";
import db from '../config/database.js'

const user_type = db.define('user_type', {
    id: {
        type: DataTypes.UUID,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING(50),
        allowNull: false
    }
}, {
    tableName: 'user_type'
})

export default user_type