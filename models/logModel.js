import { DataTypes } from "sequelize";
import db from '../config/database.js'

const log = db.define('log', {
    id: {
        type: DataTypes.UUID,
        primaryKey: true
    },
    method: {
        type: DataTypes.STRING(20),
        allowNull: false
    },
    path: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    headers: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    body: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    query: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: true
    }
}, {
    tableName: 'log'
})

export default log