import { DataTypes } from "sequelize";
import db from '../config/database.js'

const user_view = db.define('user_view', {
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
    },
    name: {
        type: DataTypes.STRING(100)
    },
    email: {
        type: DataTypes.STRING(100)
    },
    user: {
        type: DataTypes.STRING(20)
    },
    user_type_id: {
        type: DataTypes.UUID
    },
    user_type_name: {
        type: DataTypes.STRING(50)
    },
    is_active: {
        type: DataTypes.TINYINT
    },
    created_at: {
        type: DataTypes.DATE,
    },
    updated_at: {
        type: DataTypes.DATE,
    },
    created_by: {
        type: DataTypes.UUID,
    },
    updated_by: {
        type: DataTypes.UUID,
    }
}, {
    tableName: 'user_view'
})

export default user_view