import express from 'express';
import { create, index, login, show, update, destroy, deleteSoft } from '../controllers/userController.js'

const router = express.Router();

router.post('/login', login);
router.post('/', create);
router.get('/', index);
router.get('/:id', show);
router.put('/:id', update);
router.delete('/:id', destroy);
router.delete('/:id/delete-soft', deleteSoft);

export default router   