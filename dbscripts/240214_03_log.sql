CREATE TABLE `log` (
    `id` VARCHAR(36) PRIMARY KEY,
    `method` VARCHAR(20) NOT NULL,
    `path` VARCHAR(255) NOT NULL,
    `headers` TEXT NULL,
    `body` TEXT NULL,
    `query` TEXT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP()
);