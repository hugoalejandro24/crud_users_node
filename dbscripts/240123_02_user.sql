DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
    `id` VARCHAR(36) PRIMARY KEY,
    `name` VARCHAR(100) NOT NULL,
    `user` VARCHAR(20) NOT NULL,
    `user_type_id` VARCHAR(36) NOT NULL,
    `email` VARCHAR(100) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `is_active` TINYINT NOT NULL DEFAULT 1,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    `created_by` VARCHAR(36) NOT NULL DEFAULT "1",
    `updated_at` TIMESTAMP DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    `updated_by` VARCHAR(36) NULL,
    `deleted_at` TIMESTAMP NULL,
    `deleted_by` VARCHAR(36) NULL,
    FOREIGN KEY (`user_type_id`) REFERENCES `user_type`(`id`),
    CONSTRAINT `unique_user_user` UNIQUE (`user`),
    CONSTRAINT `unique_user_email` UNIQUE (`email`)
);

INSERT INTO `user` (`id`, `name`, `user`, `email`, `user_type_id`, `password`)
VALUES ((SELECT UUID()), "Amin", "admin", "admin@gmail.com", (SELECT `id` FROM `user_type` WHERE `name` = "Admin"), "123456");