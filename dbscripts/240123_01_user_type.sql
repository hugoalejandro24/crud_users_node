DROP TABLE IF EXISTS `user_type`;
CREATE TABLE `user_type` (
    `id` VARCHAR(36) PRIMARY KEY,
    `name` VARCHAR(50) NOT NULL,
    CONSTRAINT `unique_user_type_name` UNIQUE (`name`)
);

INSERT INTO `user_type` (`id`, `name`) VALUES ((SELECT UUID()), "Admin");