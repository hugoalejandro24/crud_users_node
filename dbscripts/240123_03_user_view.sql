CREATE OR REPLACE VIEW `user_view` AS 
SELECT
    `u`.`id` AS `id`,
    `u`.`name` AS `name`,
    `u`.`user` AS `user`,
    `u`.`email` AS `email`,
    `u`.`is_active` AS `is_active`,
    `u`.`user_type_id` AS `user_type_id`,
    `ut`.`name` AS `user_type_name`,
    `u`.`created_at` AS `created_at`,
    `u`.`created_by` AS `created_by`,
    `u`.`updated_at` AS `updated_at`,
    `u`.`updated_by` AS `updated_by`
FROM
    `user` `u`
INNER JOIN
    `user_type` `ut`
ON
    `u`.`user_type_id` = `ut`.`id`
WHERE
    `u`.`deleted_at` IS NULL;