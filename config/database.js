import Sequelize from "sequelize";
import CONSTANTS from "./constants.js";

const db = new Sequelize(CONSTANTS.DB_NAME, CONSTANTS.DB_USER, CONSTANTS.DB_PASSWORD, {
    host: CONSTANTS.DB_HOST,
    port: CONSTANTS.DB_PORT,
    dialect: 'mysql',
    logging: CONSTANTS.DEBUG ? true : false,
    define: {
        timestamps: false
    },
    pool: {
        max: 5,
        min: 0,
        acquire: 3000,
        idle: 10000
    },
    operatorAliases: false
});

export default db