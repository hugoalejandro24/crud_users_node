import dotenv from 'dotenv';
dotenv.config({path:'.env'});

const CONSTANTS = {
    DB_NAME: process.env.DB_NAME,
    DB_USER: process.env.DB_USER,
    DB_HOST: process.env.DB_HOST,
    DB_PORT: process.env.DB_PORT,
    DB_PASSWORD: process.env.DB_PASSWORD,
    PORT: process.env.PORT,
    DEBUG: process.env.DEBUG,
    JWT_SECRET_KEY: process.env.JWT_SECRET_KEY
}

export default CONSTANTS