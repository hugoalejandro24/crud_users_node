import express from "express";
import cors from 'cors';
import userRoutes from './routes/userRoutes.js';
import db from './config/database.js';
import { pathNotFound } from "./responses/generic_responses.js";
import jwtMiddleware from './middlewares/jwtMiddleware.js';
import logMiddleware from './middlewares/logMiddleware.js'
import CONSTANTS from "./config/constants.js";

const app = express();

try{
    await db.authenticate();
    console.log("Connection established");
} catch (error) {
    console.log(error);
}

app.use(express.json({extended:true}));

app.use(cors());

/* MIDDLEWARES */
app.use(logMiddleware);
app.use(jwtMiddleware);
/* END MIDDLEWARES */

app.use('/users', userRoutes);

app.use((req, res, next) => {
    res.status(404).json(pathNotFound("Path not found"));
})

app.listen(CONSTANTS.PORT || 3000, () => {
    console.log("listening...")
});