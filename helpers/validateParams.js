const validateParams = (dataTuples) => {
    const result = {};

    for (const [varName, varValue, expectedType, isRequired, maxSize, minSize] of dataTuples) {
        if (varValue === null || varValue === undefined) {
            if (isRequired) {
                result.error = `${varName} required and can't be null.`;
            }
            continue;
        }

        if (expectedType === 'string') {
            if (typeof varValue !== 'string') {
                result.error = `${varName} must be a string.`;
            } else if ((minSize !== undefined && varValue.length < minSize) || (maxSize !== undefined && varValue.length > maxSize + 1)) {
                result.error = `${varName} must have a length between ${minSize} and ${maxSize}.`;
            }

        } else if (expectedType === 'number') {
            if (typeof varValue !== 'number') {
                result.error = `${varName} must be a number.`;
            } else if ((minSize !== undefined && varValue < minSize) || (maxSize !== undefined && varValue > maxSize + 1)) {
                result.error = `${varName} must have a value between ${minSize} and ${maxSize}.`;
            }

        } else if (expectedType === 'is_active') {
            if (varValue != "0" && varValue != "1") {
                result.error = `${varName} must have a value between 0 and 1.`;
            }

        } else {
            result.error = `${varName} has an unsupported type.`;
        }

        if (result.error) {
            break;
        }

        result[varName] = varValue;
    }

    return result;
}

export default validateParams;