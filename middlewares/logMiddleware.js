import logModel from '../models/logModel.js';
import { v4 as uuidv4 } from 'uuid';

const log = async (req, res, next) => {
    await logModel.create({
        id: uuidv4(),
        method: req.method,
        path: (req.baseUrl + (req.path != "/" ? req.path : "")),
        body: req.body ? JSON.stringify(req.body) : null,
        query: req.query ? JSON.stringify(req.query) : null,
        headers: req.headers ? JSON.stringify(req.headers) : null
    });
    next();
}

export default log