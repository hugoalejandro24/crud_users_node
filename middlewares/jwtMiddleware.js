import jwt from 'jsonwebtoken';
import userViewModel from '../models/userViewModel.js';
import { missingToken, invalidToken } from '../responses/generic_responses.js';
import CONSTANTS from "../config/constants.js";

const jwtMiddleware = async (req, res, next) => {
    if (allowPaths.includes(req.baseUrl + (req.path != "/" ? req.path : ""))) return next();
    let token = req.header('Authorization');
    if (!token) return res.status(401).json(missingToken());
    token = token.replace('Bearer ', '');
    try {
        const decoded = jwt.verify(token, CONSTANTS.JWT_SECRET_KEY);
        req.user = await userViewModel.findOne({where:{id:decoded.id, is_active:1}})
        if (!req.user) throw new Error("User not found")
        return next();
    } catch (error) {
        return res.status(401).json(invalidToken(CONSTANTS.DEBUG ? error : null));
    }
};

const allowPaths = [
    "/users/login"
]

export default jwtMiddleware