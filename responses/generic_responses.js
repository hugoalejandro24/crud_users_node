const invalidParams = (text, data = null) => {
    return {
        status: false,
        http_status_code: 400,
        title: "Invalid Params",
        text,
        data
    };
};

const missingToken = () => ({
    status: false,
    http_status_code: 401,
    title: "Missing token"
});

const invalidToken = (data = null) => ({
    status: false,
    http_status_code: 401,
    title: "Invalid token",
    data
});

const pathNotFound = (text) => ({
    status: false,
    http_status_code: 404,
    title: "Not found",
    text,
    data: null
});

const notFound = (text, data = null) => ({
    status: false,
    http_status_code: 404,
    title: "Resource not found",
    text,
    data,
});

const serverError = (text, data = null) => ({
    status: false,
    http_status_code: 500,
    title: "Server error",
    text,
    data:(CONSTANTS.DEBUG) ? data : null,
});

export {
    invalidParams,
    missingToken,
    invalidToken,
    pathNotFound,
    notFound,
    serverError
}