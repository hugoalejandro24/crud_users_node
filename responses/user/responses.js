const successIndex = (data = null) => {
    return {
        status: true,
        http_status_code: 200,
        title: "Success Index",
        text: "User index",
        data
    };
};

const successShow = (data = null) => {
    return {
        status: true,
        http_status_code: 200,
        title: "Success Show",
        text: "User show",
        data
    };
};

const successLogin = (data = null, token) => {
    return {
        status: true,
        http_status_code: 201,
        title: "Success Login",
        text: "User login",
        data,
        Authorization:token
    };
};
const notFound = (data = null) => {
    return {
        status: false,
        http_status_code: 404,
        title: "Resource not found",
        text: "User not found",
        data
    };
};

const successCreate = (data = null) => {
    return {
        status: true,
        http_status_code: 201,
        title: "Success Create",
        text: "User create",
        data
    };
};
const successUpdate = (data = null) => {
    return {
        status: true,
        http_status_code: 201,
        title: "Success Update",
        text: "User update",
        data
    };
};

const successDelete = (data = null) => {
    return {
        status: true,
        http_status_code: 201,
        title: "Success Delete",
        text: "User delete",
        data
    };
};

const failCreate = (data = null) => {
    return {
        status: false,
        http_status_code: 500,
        title: "Fail Create",
        text: "User create",
        data
    };
};

const failLogin = (data = null) => {
    return {
        status: false,
        http_status_code: 401,
        title: "Fail login",
        text: "User login",
        data
    };
};

export {
    successIndex,
    successShow,
    successLogin,
    successCreate,
    failCreate,
    successUpdate,
    successDelete,
    failLogin,
    notFound
}